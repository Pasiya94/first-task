/**
 * ContactsController
 *
 * @description :: Server-side logic for managing contacts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  
  GetAllContacts: function (req,res) {
    
    ContactService.GetAllContacts(function(err, records) {
    if (!err) {
      return res.json(records);
    }else{
      return res.notFound();
    }
    });    
  },

   AddContactDetails: function (req, res) {

    ContactService.AddContactDetails({name: req.param('name'),phoneNumber:req.param('phoneNumber')},function(err,message) {
    if (!err) {
      return res.send(message);
    }else{
      return res.notFound();
    }
   });       
 }
};
